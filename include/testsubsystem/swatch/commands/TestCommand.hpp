#ifndef __TESTSUBSYSTEM_SWATCH_COMMANDS_TESTCOMMAND_HPP__
#define __TESTSUBSYSTEM_SWATCH_COMMANDS_TESTCOMMAND_HPP__

#include "swatch/action/Command.hpp"

namespace testsubsystem {
namespace swatch {
namespace commands {

// This is a test command
class TestCommand : public ::swatch::action::Command {
public:
  TestCommand(const std::string&, ::swatch::action::ActionableObject&);

  virtual ~TestCommand();

private:
  virtual State code(const ::swatch::core::ParameterSet&);
};

} // namespace commands
} // namespace swatch
} // namespace testsubsystem

#endif 