#ifndef __TESTSUBSYSTEM_SWATCH_ALGORITHM_HPP__
#define __TESTSUBSYSTEM_SWATCH_ALGORITHM_HPP__

#include "swatch/phase2/AlgoInterface.hpp"


namespace testsubsystem {
namespace swatch {

// Represents algorithm/payload firmware
class Algorithm : public ::swatch::phase2::AlgoInterface {
public:
  Algorithm();
  ~Algorithm();

private:
  void retrieveMetricValues() final;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&) representing
  //        your monitoring data for the algorithm/payload firmware
};

} // namespace swatch
} // namespace testsubsystem


#endif