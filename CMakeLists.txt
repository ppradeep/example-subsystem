
cmake_minimum_required(VERSION 3.17.5 FATAL_ERROR)
project(testsubsystem-swatch VERSION 0.4.0)


find_package(swatch 1.8.4 REQUIRED)
find_package(emp-swatch 0.4.20 REQUIRED)
find_package(serenity-swatch 0.4.1 REQUIRED)


#############################################
# Plugin library: Source files & dependencies
#############################################

add_library(testsubsystem_swatch SHARED)

target_sources(testsubsystem_swatch
    PRIVATE
        src/common/commands/ConfigureComponentX.cpp
        src/common/commands/LoadLUT.cpp
        src/common/commands/TestCommand.cpp
        src/common/commands/SetThresholds.cpp
        src/common/Algorithm.cpp
        src/common/Processor.cpp
    )

target_include_directories(testsubsystem_swatch
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>
    )

target_link_libraries(testsubsystem_swatch
    PUBLIC
        SWATCH::swatch_action
        SWATCH::swatch_phase2
        SERENITY::serenity_swatch
    )


#######################################################
# Main target: Build options
#######################################################

target_compile_features(testsubsystem_swatch PUBLIC cxx_std_14)
target_compile_options(testsubsystem_swatch PRIVATE -g -fPIC -O3 -Werror=return-type)


#######################################################
# Default rules (install, packaging, clang-format etc.)
#######################################################

include(${swatch_CMAKE_DIR}/../cactus-utilities.cmake)
set(PLUGIN_CONFIG_FILE herd.yml)
cactus_add_phase2_plugin_rules(
    EXAMPLE_SUBSYSTEM
    testsubsystem_swatch
    "Boost 1.53.0 REQUIRED COMPONENTS chrono filesystem regex thread unit_test_framework"
)
