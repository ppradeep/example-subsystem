#include "testsubsystem/swatch/commands/TestCommand.hpp"


#include "testsubsystem/swatch/Processor.hpp"
#include <iostream>

namespace testsubsystem {
namespace swatch {
namespace commands {

using namespace ::swatch;

TestCommand::TestCommand(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Run a test command", aActionable, std::string())
{  
}

TestCommand::~TestCommand()
{
}

action::Command::State TestCommand::code(const core::ParameterSet& params)
{
  setStatusMsg("Running test command");
  setProgress(0., "Starting command");
  setProgress(1., "Finishing command");
  setStatusMsg("Finishing test command");

  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace testsubsystem
