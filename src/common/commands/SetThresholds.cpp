#include "testsubsystem/swatch/commands/SetThresholds.hpp"


#include "testsubsystem/swatch/Processor.hpp"


namespace testsubsystem {
namespace swatch {
namespace commands {

using namespace ::swatch;


SetThresholds::SetThresholds(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Set algorithm thresholds", aActionable, std::string())
{
  // Three thresholds for the algorithm (two different types)
  registerParameter<uint32_t>("threshold1", 0);
  registerParameter<uint32_t>("threshold2", 0);
  registerParameter<uint64_t>("anotherThreshold", 0);
  // FIXME: Modify names, types & multiplicity of parameters as appropriate
}


SetThresholds::~SetThresholds()
{
}


action::Command::State SetThresholds::code(const core::ParameterSet& params)
{
  // FIXME: Call functions from that writes the thresholds declared above into registers in the algorithm
  //        Access parameters as follows: params.get<TYPE>("myParameter")
  //        If this command takes longer than a few seconds, declare progress using setProgress member function


  // FIXME: Return State::kError if error occurred, State::kDone if successful, or
  //        State::kWarning if successful overall but something abnormal and worrisome happened along the way
  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace testsubsystem
