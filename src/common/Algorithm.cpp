#include "testsubsystem/swatch/Algorithm.hpp"


namespace testsubsystem {
namespace swatch {

using namespace ::swatch;

Algorithm::Algorithm() :
  AlgoInterface()
// FIXME: Register metrics for monitoring data here
{
}


Algorithm::~Algorithm()
{
}


void Algorithm::retrieveMetricValues()
{
  // FIXME: Read monitoring data, and set values of corresponding metrics (in long term)
}

} // namespace swatch
} // namespace testsubsystem
