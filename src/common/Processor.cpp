#include "testsubsystem/swatch/Processor.hpp"

#include "swatch/core/Factory.hpp"

#include "testsubsystem/swatch/Algorithm.hpp"
#include "testsubsystem/swatch/commands/ConfigureComponentX.hpp"
#include "testsubsystem/swatch/commands/LoadLUT.hpp"
#include "testsubsystem/swatch/commands/TestCommand.hpp"
#include "testsubsystem/swatch/commands/SetThresholds.hpp"
#include "swatch/action/StateMachine.hpp"


SWATCH_REGISTER_CLASS(testsubsystem::swatch::Processor)

namespace testsubsystem {
namespace swatch {

using namespace ::swatch;

Processor::Processor(const ::swatch::core::AbstractStub& aStub) :
  serenity::swatch::DaughterCard(aStub)
{
  // 1) Declare commands
  registerCommand<commands::ConfigureComponentX>("configureComponentX");
  registerCommand<commands::LoadLUT>("loadLUT");
  registerCommand<commands::SetThresholds>("setThresholds");
  registerCommand<commands::TestCommand>("testCommand");
  // FIXME: Rename the above commands as appropriate for your subsystem (and then add any others that you need)

  // 2) Declare FSMs
  // FIXME: Add the above commands to transitions of pre-defined FSMs where applicable.
  // Test FSM with default states
  swatch::action::StateMachine& testFSM = registerStateMachine("testFSM", "Halted", "Error");
  testFSM.addState("TestState");
  testFSM.addTransition("testtransition", "Halted", "TestState").add(getCommand("testCommand"));
  // 3) Declare class representing algorithm/payload firmware
  registerAlgo(new Algorithm());
}


Processor::~Processor()
{
}

} // namespace swatch
} // namespace testsubsystem
