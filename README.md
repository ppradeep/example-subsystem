# Example subsystem SWATCH plugin

This repository contains an example plugin for phase-2 subsystems, designed to be used as a basis
for creating plugins for real subsystems.

This plugin contains an example specialisation of the processor and algorithm classes, along
with three example 'command' classes (for configuration procedures). The class implementations
in this repository are all minimal skeletons, with placholders and other important sections
highlighted by 'FIXME' comments. For detailed instructions on how to create a real plugin from
this example, please read the "Creating a board plugin" page from the plugin development guide
at https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development


## Dependencies

The plugin library has one direct dependency, the [exampleboard plugin](https://gitlab.cern.ch/cms-cactus/phase2/software/plugins/exampleboard/),
which itself builds on the following packages:
 * [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch)
 * [HERD](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-app)


## Build

To build the plugin, firstly clone this repository:
```sh
git clone ssh://git@gitlab.cern.ch:7999/cms-cactus/phase2/software/plugins/examplesubsystem.git
```

Then follow [these instructions](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/development-environment.html)
for setting up your development environment. Note: Using the VSCode Dev Containers method from
that page is recommended.

After you've set up your development environment, run the standard CMake build workflow to build the plugin:
```sh
mkdir build
cd build
cmake3 ..
make -j$(nproc)
```
